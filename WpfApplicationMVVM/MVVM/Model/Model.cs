﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationMVVM.MVVM.Model
{
    public class NotifyBase : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        /*
         En WPF debemos crear las propiedades como 
         DependencyProperties en los modelos, esto para realizar 
         nuestros Bindings en las vistas.
         Vamos a agregar métodos OnPropertyChanged específicos para cada 
         propiedad, sin embargo 
         generalmente solo se necesita que se notifique a la vista 
         cualquier cambio en las propiedades, es por esto que se puede 
         realizar una clase padre que pase por parámetro el nombre de la 
         propiedad y realice la notificación como se muestra a continuación, 
         con esta implementación los modelos heredan de esta clase y llaman 
         el método OnPropertyChanged en cada set.
             
             */

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}