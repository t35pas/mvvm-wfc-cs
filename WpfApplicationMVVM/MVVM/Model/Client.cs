﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplicationMVVM.MVVM.Model;

namespace WpfApplicationMVVM.Model
{
    class Client : NotifyBase
    {

        #region Atributos
        private int id;
        private string name;
        private string lastname;
        #endregion

        #region Propiedades
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                //Notifica cambios de propiedades en la VISTA.
                OnPropertyChanged("Id");
                OnPropertyChanged("DisplayName");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
                OnPropertyChanged("DisplayName");
            }
        }

        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
                OnPropertyChanged("LastName");
                OnPropertyChanged("DisplayName");
            }
        }

        public string DisplayName
        {
            get
            {
                return Id + "-" + Name + " " + LastName;
            }
        }
        #endregion

    }
}