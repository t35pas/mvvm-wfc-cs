﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfApplicationMVVM.Model;
using WpfApplicationMVVM.MVVM.ViewModel;

namespace WpfApplicationMVVM.MVVM.ViewModel
{
    class ClientViewModel : ObservableCollection<Client>, INotifyPropertyChanged
    {

        #region Atributos
        private int selectedIndex;

        private int id;
        private string name;
        private string lastName;
        private ICommand addClientCommand;
        private ICommand clearCommand;
        #endregion

        #region Propiedades
        public int SelectedIndexOfCollection
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
                OnPropertyChanged("SelectedIndexOfCollection");

                //Activa el evento OnPropertyChanged de los atributos para 
                //refrescar las propiedades segun el indice seleccionado.
                OnPropertyChanged("Id");
                OnPropertyChanged("Name");
                OnPropertyChanged("LastName");
            }
        }

        public int Id
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Id;
                }
                else
                {
                    return id;
                }
            }
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Id = value;
                }
                else
                {
                    id = value;
                }
                OnPropertyChanged("Id");
            }
        }

        public string Name
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Name;
                }
                else
                {
                    return name;
                }
            }
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Name = value;
                }
                else
                {
                    name = value;
                }
                OnPropertyChanged("Name");
            }
        }

        public string LastName
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].LastName;
                }
                else
                {
                    return lastName;
                }
            }
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].LastName = value;
                }
                else
                {
                    lastName = value;
                }
                OnPropertyChanged("LastName");
            }
        }

        public ICommand AddClientCommand
        {
            get
            {
                return addClientCommand;
            }
            set
            {
                addClientCommand = value;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                return clearCommand;
            }
            set
            {
                clearCommand = value;
            }
        }
        #endregion

        #region Constructores
        public ClientViewModel()
        {
            Client vlClient1 = new Client();
            vlClient1.Id = 1;
            vlClient1.Name = "Pablo";
            vlClient1.LastName = "Gonzalez";
            this.Add(vlClient1);

            Client vlClient2 = new Client();
            vlClient2.Id = 2;
            vlClient2.Name = "Roberto";
            vlClient2.LastName = "Herrera";
            this.Add(vlClient2);

            Client vlClient3 = new Client();
            vlClient3.Id = 3;
            vlClient3.Name = "Anibal";
            vlClient3.LastName = "Salazar";
            this.Add(vlClient3);

            //Formas de crear Command

            AddClientCommand = new CommandBase(param => this.AddClient());
            ClearCommand = new CommandBase(new Action<Object>(ClearClient));
        }
        #endregion

        #region Interface
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Metodos/Funcciones
        private void AddClient()
        {
            Client vlClient = new Client();
            vlClient.Id = Id;
            vlClient.Name = Name;
            vlClient.LastName = LastName;
            this.Add(vlClient);
        }

        private void ClearClient(object obj)
        {
            SelectedIndexOfCollection = -1;
            Id = 0;
            Name = "";
            LastName = "";
        }
        #endregion
    }
}